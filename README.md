# Trunk Based Development

[![pipeline status](https://gitlab.com/gsalaun1/tbd/badges/main/pipeline.svg)](https://gitlab.com/gsalaun1/tbd/-/commits/main)

## Deployment

Main branch : https://gsalaun1.gitlab.io/tbd

Other branches : "View app" in Merge Request

![](mr.png)